# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import render_template,request, redirect
from pepperchips import app
from datetime import datetime
from include import db
from course.include import cdb

@app.route('/install.py', methods=['GET', 'POST'])
def install():
	if request.method == "POST":
		install_confirm()
		return redirect('/')
	return render_template('install.tpl')

def install_confirm():
	db.DB.drop_all()
	cdb.DB.drop_all()
	db.DB.create_all()
	cdb.DB.create_all()
	u = db.adduser(-1, request.form.get('username'), request.form.get('password'), request.form.get('email'), 5)
	cat = db.cat_add("cat 1")
	db.cat_add("cat 2")
	db.cat_add("cat 3")
	db.cat_add("cat 4")
	p = db.Post()
	p.title = "my first post"
	p.postdata = "This is a test post for testing pepperchips functionality."
	p.date = datetime.now()
	p.flags = 0
	p.uid = 1
	p.catid = 1
	db.DB.session.add(p)
	db.DB.session.commit()
	
	db.addcomment(u, p, "Awesome project dude! keep it up.")
	
	box = db.addbox("header", 0, 0)
	db.addlink(box, "Home", "/index.py")
	db.addlink(box, "Courses", "/course/")
	db.addlink(box, "Install", "/install.py")
	
	box = db.addbox("user control", 1, 2)
	db.addlink(box, "New post", "/new.py")
	db.addlink(box, "New user", "/new.py?action=user")
	db.addlink(box, "New link", "/new.py?action=link")
	db.addlink(box, "New course", "/course/new.py")
	db.addlink(box, "New Semester", "/course/new.py?action=semester")
	db.addlink(box, "New entrollment", "/course/new.py?action=enrollment")
	
	box = db.addbox("links", 0, 3)
	db.addlink(box, "Google", "//google.com")
	db.addlink(box, "Microsoft", "//microsoft.com")
	db.addlink(box, "motherfuckingwebsite", "http://motherfuckingwebsite.com")
	db.addlink(box, "bettermotherfuckingwebsite", "http://bettermotherfuckingwebsite.com")

	box = db.addbox("Categories", 0, 4, "SPECIAL::category")
	
	sem = cdb.addsem(962)
	
