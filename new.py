# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import render_template, session, request, redirect
from datetime import datetime
from pepperchips import app
from include import db, util

@app.route('/new.py', methods=['GET', 'POST'])
def new():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	if loggeduser == None:
		return redirect("/login.py?logout")
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	
	if db.user_getlevel(loggeduser) < 5:
		return "403", 403
	form = dict()
	action = request.args.get("action", "post")
	if action == "link":
		form['action'] = "link"
	elif action == "user":
		form['action'] = "user"
	else:
		form['action'] = "post"
		
	if request.method == "POST":
		if form['action'] == "post":
			return new_dopost(loggeduser, siteopt, linkarr)
		elif form['action'] == "link":
			return new_dolink(loggeduser, siteopt, linkarr)
		elif form['action'] == "user":
			return new_douser(loggeduser, siteopt, linkarr)

	return render_template('new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def new_dopost(loggeduser, siteopt, linkarr):
	text = request.form.get('text')
	title = request.form.get('title')
	catid = int(request.form.get('cat'))
	page = request.form.get('page', 0)
	if page != 0:
		page = 1
	post = db.addpost(user = loggeduser, title = title, postdata = text, catid = catid, date = datetime.now(), page = page)
	form = dict()
	form['error_num'] = 1
	form['error'] = "OK"
	form['title'] = title
	form['text'] = text
	form['cat'] = str(catid)
	form['page'] = page
	form['action'] = "post"
	if page != 0:
		box = db.findbox_byid(1)
		db.addlink(box = box, title = title, link = "SPECIAL::POST::" + str(db.post_getid(post)), pos = -1)
	return render_template('new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def new_dolink(loggeduser, siteopt, linkarr):
	title = request.form.get('title')
	boxid = int(request.form.get('box'))
	pos = int(request.form.get('pos'))
	link = request.form.get('link')
	
	form = dict()
	form['error_num'] = 1
	form['title'] = title
	form['box'] = boxid
	form['pos'] = pos
	form['link'] = link
	form['action'] = "link"
	
	box = db.findbox_byid(boxid)
	if box == None:
		form['error'] = "Invalid box id"
		return render_template('new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)
	
	db.addlink(box = box, title = title, link = link, pos = pos)
	form['error'] = "OK"
	return render_template('new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def new_douser(loggeduser, siteopt, linkarr):
	username = request.form.get('username', "")
	password = request.form.get('password', "")
	password2 = request.form.get('password2', "")
	email = request.form.get('email', "")
	level = request.form.get('level', "")
	uid = request.form.get('uid', -1)
	
	form = dict()
	form['error_num'] = 1
	form['action'] = "user"
	form['username'] = username
	form['password'] = password
	form['password2'] = password2
	form['email'] = email
	form['level'] = level
	form['uid'] = uid
	if uid == "":
		uid = -1;
		
	if username == "" or password == "" or password2 == "" or email == "" or level == "":
		form['error'] = "Fill in the required forms."
	elif password != password2:
		form['error'] = "Passwords don't match"
	else:
		u = db.adduser(uid, username, password, email, level)
		if u == None:
			form['error'] = "Failed to create user."
		else:
			form['error'] = "OK"
		
	return render_template('new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)
