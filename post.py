# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import render_template, session, request, redirect
from pepperchips import app
from include import db, util

@app.route('/post.py', methods=['GET', 'POST'])
def post():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	if loggeduser == None:
		return redirect("/login.py?logout")
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	pid = request.args.get('pid', None)
	if pid == None:
		return redirect('/index.py')
	p = db.listpost_byid(pid, db.user_getlevel(loggeduser))
	if p == None:
		return "404", 404
	
	comments = db.listcomments(pid, db.user_getlevel(loggeduser))
	commentarr = list()
	for c in comments:
		user = db.finduser_byid(c.uid)
		commentarr.append({'username': user.username, 'uid': c.uid, 'date': c.date, 'commentdata': c.commentdata })
	postarr = list()
	postarr.append({ 'title': p['title'], 'postdata': p['postdata'], 'date': p['date'], 'username': p['username'] , 'uid': p['uid'], 'url': "post.py?pid=" + str(p['pid']), 'cat': p['category'] })
	if request.method == "POST":
		return post_comment(loggeduser, siteopt, linkarr, p)
	
	return render_template("post.tpl", siteopt = siteopt, postarr = postarr, linkarr = linkarr, commentarr = commentarr)

def post_comment(loggeduser, siteopt, linkarr, p):
	post = db.findpost_byid(p['pid'])
	if post == None:
		return "404", 404
	
	if request.form.get('comment', '') == '':
		return "400", 400
	
	db.addcomment(user = loggeduser, post = post, text = request.form.get('comment', ''))
	postarr = list()
	postarr.append(p)
	comments = db.listcomments(p['pid'], db.user_getlevel(loggeduser))
	commentarr = list()
	for c in comments:
		user = db.finduser_byid(c.uid)
		commentarr.append({'username': user.username, 'uid': c.uid, 'date': c.date, 'commentdata': c.commentdata })
		
	return render_template("post.tpl", siteopt = siteopt, postarr = postarr, linkarr = linkarr, commentarr = commentarr)
	
