# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import render_template, session, request, redirect
from pepperchips import app
from datetime import datetime
from include import db, util

@app.route('/login.py', methods=['GET', 'POST'])
def login():
	form = {'username': "", 'error': ""}
	if(request.method == "POST"):
		if request.form.get('submit') == 'login':
			form['username'] = request.form.get('username')
			user = db.finduser_byname(request.form.get('username'))
			if user != None and util.checkpassword(request.form.get('password'), user.password):
				session['logged_in'] = 1
				session['uid'] = user.uid
				session['username'] = user.username
			else:
				form['error'] = "Bad Username/Password"
				form['error_num'] = 1
				session['logged_in'] = 0
		elif request.form.get('submit') == 'register':
			if request.form.get('password') == request.form.get('password2'):
				u = db.adduser(-1, request.form.get('username'), request.form.get('password'), request.form.get('email'), 1);
				session['logged_in'] = 1
				session['uid'] = u.uid
				session['username'] = u.username
			else:
				form['error'] = "Passwords don't match"
				form['error_num'] = 2
		else:
			print("bad submit value")
	else:
		if 'logout' in request.args:
			session['logged_in'] = 0
			return redirect('/login.py')
	
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	if loggeduser == None:
		return redirect("/login.py?logout")
	elif db.user_getlevel(loggeduser) > 0:
		return redirect("/")
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	return render_template("login.tpl", form = form, siteopt = siteopt, linkarr = linkarr)
