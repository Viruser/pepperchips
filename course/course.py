# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, session, request, redirect
import os
capp = Blueprint('course', __name__)

from course import index, new, assignment

from pepperchips.include import db, util
from course.include import cdb

@capp.route("/course.py", methods=['GET', 'POST'])
def course():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	if loggeduser.uid == -1:
		return redirect("../login.py")
	csid = request.args.get("csid", -1)
	cs = cdb.findcourse_byid(csid)
	if cs == None:
		return "Unknown course id.", 404
	siteopt['csid'] = csid
	if db.user_getid(loggeduser) == cs.ownerid:
		siteopt['courseowner'] = 1
	elif loggeduser.level >= 5:
		pass
	else:
		if cdb.checkenrollment(db.user_getid(loggeduser), csid) == 0:
			return "You are not enrolled in this class.", 403
	post = dict()
	siteopt['page'] = request.args.get("action", "news")
	if siteopt['page'] == "grades":
		if cs.grades == None:
			post['text'] = "No grades have been posted yet."
		else:
			post['text'] = cs.grades

	elif siteopt['page'] == "assignments":
		if cs.assignments == None:
			post['text'] = "No assignment"
		else:
			post['text'] = cs.assignments
			siteopt['form'] = 1
		if request.method == "POST" and siteopt['form']:
			return course_uploadassignment(loggeduser, siteopt, linkarr, post)
	elif siteopt['page'] == "news":
		return course_news(loggeduser, siteopt, linkarr)
	
	return render_template('course/course.tpl', siteopt = siteopt, linkarr = linkarr, post = post)

def course_news(loggeduser, siteopt, linkarr):
	postarr = cdb.listcpost(siteopt['csid'])
	return render_template('course/course.tpl', siteopt = siteopt, linkarr = linkarr, postarr = postarr)

def course_uploadassignment(loggeduser, siteopt, linkarr, post):
	form = dict()
	if 'assignment' not in request.files or request.files['assignment'].filename == "":
		form['error'] = "No file was uploaded."
	else:
		reqfile = request.files['assignment']
		path = os.path.join("/tmp", reqfile.filename)
		reqfile.save(path)
		asm = cdb.newassignment(siteopt['csid'], db.user_getid(loggeduser), path)
		if asm == None:
			form['error'] = "Upload failed."
		else:
			form['error'] = "OK"
		
	return render_template('course/course.tpl', siteopt = siteopt, linkarr = linkarr, post = post, form = form)
