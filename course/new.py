# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.
from flask import render_template, session, request, redirect
from pepperchips.include import db, util
from course.course import capp
from course.include import cdb

@capp.route('/new.py',  methods=['GET', 'POST'])
def cnew():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	if loggeduser == None:
		return redirect("/login.py?logout")
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	form = dict()
	form['action'] = request.args.get("action", "course")
	csid = request.args.get("csid", -1)
	cs = cdb.findcourse_byid(csid)
	if not cs == None:
		siteopt['csid'] = csid
		if db.user_getid(loggeduser) == cs.ownerid:
			siteopt['courseowner'] = 1
	
	if form['action'] == "course" or form['action'] == "enrollment" or form['action'] == "semester":
		if db.user_getlevel(loggeduser) < 5:
			return "403", 403
	elif form['action'] == "news":
		course = cdb.findcourse_byid(request.args.get("csid", -1))
		if course == None:
			return "Course not found.", 404
		if db.user_getid(loggeduser) != course.ownerid:
			return "You do not have permission to do that.", 403
		nid = request.args.get("nid", -1)
	elif form['action'] == "assignments":
		course = cdb.findcourse_byid(request.args.get("csid", -1))
		if course == None:
			return "Course not found.", 404
		if db.user_getid(loggeduser) != course.ownerid:
			return "You do not have permission to do that.", 403
		form['text'] = course.assignments;
	elif form['action'] == "grades":
		course = cdb.findcourse_byid(request.args.get("csid", -1))
		if course == None:
			return "Course not found.", 404
		if db.user_getid(loggeduser) != course.ownerid:
			return "You do not have permission to do that.", 403
		form['text'] = course.grades;
	else:
		return "Unknown action.", 400
	siteopt['csid'] = request.args.get("csid", -1)
	if request.method == "POST":
		if form['action'] == "course":
			return do_cnew(loggeduser, siteopt, linkarr, form)
		elif form['action'] == "news":
			return do_cpost(loggeduser, siteopt, linkarr, form)
		elif form['action'] == "assignments":
			return do_cassign(loggeduser, siteopt, linkarr, form)
		elif form['action'] == "grades":
			return do_cgrade(loggeduser, siteopt, linkarr, form)
		elif form['action'] == "enrollment":
			return do_cen(loggeduser, siteopt, linkarr, form)
		elif form['action'] == "semester":
			return do_csem(loggeduser, siteopt, linkarr, form)

	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def do_cnew(loggeduser, siteopt, linkarr, form):
	ownerid = request.form.get('oid', None)
	smid = request.form.get('smid', None)
	title = request.form.get('title', None)
	form['oid'] = ownerid
	form['smid'] = smid
	form['title'] = title
	if ownerid == None or smid == None or title == None:
		form['error'] = "Fill in required fields"
	cs = cdb.addcourse(title, smid, ownerid)
	if cs == None:
		form['error'] = "Check professor id and/or semester id"
	else:
		form['error'] = "OK"
	form['error_num'] = 1
	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def do_cpost(loggeduser, siteopt, linkarr, form):
	course = cdb.findcourse_byid(request.args.get("csid", -1))
	title = request.form.get("title", None)
	text = request.form.get("text", None)
	nid = request.form.get("nid", -1)
	
	if nid == "":
		nid = -1
	if title == None or text == None or course == None:
		form['error'] = "Fill the required fields."
	else:
		post = cdb.addcpost(nid, course.csid, title, text)
		if post == None:
			form['error'] = "Post failed."
		else:
			form['error'] = "OK"
	form['error_num'] = 1
	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def do_cassign(loggeduser, siteopt, linkarr, form):
	form['text'] = request.form.get('text', None);
	if form['text'] == "":
		form['text'] = None
	cdb.course_setassign(siteopt['csid'], form['text'])
	form['error'] = "OK"
	form['error_num'] = 1	
	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def do_cgrade(loggeduser, siteopt, linkarr, form):
	form['text'] = request.form.get('text', None);
	if form['text'] == "":
		form['text'] = None
	cdb.course_setgrade(siteopt['csid'], form['text'])
	form['error'] = "OK"
	form['error_num'] = 1	
	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def do_cen(loggeduser, siteopt, linkarr, form):
	form['uid'] = request.form.get('uid', -1);
	if form['uid'] == "":
		form['uid'] = -1
	form['csid'] = request.form.get('csid', -1);
	if form['csid'] == "":
		form['csid'] = -1
	user = db.finduser_byid(form['uid'])
	cs = cdb.findcourse_byid(form['csid'])
	if user == None or cs == None:
		form['error'] = "No such user or course"
		
	cdb.addenrollment(form['csid'], form['uid'])
	form['error'] = "OK"
	form['error_num'] = 1	
	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)

def do_csem(loggeduser, siteopt, linkarr, form):
	form['title'] = request.form.get('title', None);
	if form['title'] == None or form['title'] == "":
		form['error'] = "Enter valid semester number"
	else:
		sem = cdb.addsem(int(form['title']))
		if sem == None:
			form['error'] = "Failed to add semester"
		else:
			form['error'] = "OK"
			
	form['error_num'] = 1	
	return render_template('/course/new.tpl', siteopt = siteopt, linkarr = linkarr, form = form)
