# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.
from pepperchips.include.db import DB
from pepperchips.include import db
from datetime import datetime

class Course(DB.Model):
	csid = DB.Column(DB.Integer, primary_key=True)
	title = DB.Column(DB.String(256), nullable=False)
	visible = DB.Column(DB.Integer, nullable=False)
	smid = DB.Column(DB.Integer, nullable=False)
	ownerid = DB.Column(DB.Integer, nullable=False)
	grades = DB.Column(DB.Text, nullable=True)
	assignments = DB.Column(DB.Text, nullable=True)

class Semester(DB.Model):
	smid = DB.Column(DB.Integer, primary_key=True)
	number = DB.Column(DB.Integer, nullable=False)
	visible = DB.Column(DB.Integer, nullable=False)
	
class Assignment(DB.Model):
	aid = DB.Column(DB.Integer, primary_key=True)
	csid = DB.Column(DB.Integer, nullable=False)
	uid = DB.Column(DB.Integer, nullable=False)
	path = DB.Column(DB.Text, nullable=False)
	date = DB.Column(DB.DateTime, nullable=False)
	read = DB.Column(DB.Integer, nullable=False)

class CoursePost(DB.Model):
	pid = DB.Column(DB.Integer, primary_key=True)
	title = DB.Column(DB.String(256), nullable=False)
	postdata = DB.Column(DB.Text, nullable=False)
	date = DB.Column(DB.DateTime, nullable=False)
	lastedit = DB.Column(DB.DateTime, nullable=True)
	csid = DB.Column(DB.Integer, nullable=False)
	visible = DB.Column(DB.Integer, nullable=False)
	
class Enrollment(DB.Model):
	eid = DB.Column(DB.Integer, primary_key=True)
	csid = DB.Column(DB.Integer, nullable=False)
	uid = DB.Column(DB.Integer, nullable=False)

def findsem_byid(smid):
	return Semester.query(Semester.smid == smid).first()
	
def semlist(visible = 1):
	semarr = list()
	if visible == 1:
		semlist = Semester.query.filter(Semester.visible == 1).order_by(Semester.number.desc());
	else:
		semlist = Semester.query.order_by(Semester.number.desc());
	for s in semlist:
		semarr.append({"id": s.smid, "number": s.number, "visible": s.visible});
	return semarr;

def addsem(number):
	sem = Semester();
	sem.visible = 1;
	sem.number = number;
	DB.session.add(sem)
	DB.session.commit()
	return sem
	
def courselist(smid, visible = 1):
	csarr = list()
	if visible == 1:
		cslist = Course.query.filter(Course.smid == smid).filter(Course.visible == 1).all()
	else:
		cslist = Course.filter(Course.smid == smid).query.all()
	for cs in cslist:
		owner = db.finduser_byid(cs.ownerid)
		if owner != None:
			csarr.append({"id": cs.csid, "smid": cs.smid, "title": cs.title, "ownerid": cs.ownerid, "owner": db.user_getname(owner), "visible": cs.visible})
	return csarr;

def addcourse(title, smid, ownerid):
	cs = Course()
	cs.smid = smid
	cs.ownerid = ownerid
	cs.title = title
	cs.visible = 1
	owner = db.finduser_byid(ownerid)
	if owner == None or owner.level < 2 or findsem_byid == None:
		return None
	DB.session.add(cs)
	DB.session.commit()
	return cs

def findcourse_byid(csid):
	return Course.query.filter(Course.csid == csid).first()

def addcpost(pid, csid, title, text):
	if pid != -1:
		post = CoursePost.query.filter(CoursePost.pid == pid).first()
		if post == None:
			return None
		post.lastedit = datetime.now()
	else:
		post = CoursePost()
		post.date = datetime.now()
	if findcourse_byid(csid) == None:
		return None
	post.csid = csid
	post.title = title
	post.postdata = text
	post.visible = 1
	if pid == -1:
		DB.session.add(post)
	DB.session.commit()
	return post

def listcpost(csid, visible = 1):
	if visible == 0:
		cpost = CoursePost.query.filter(CoursePost.csid == csid).order_by(CoursePost.date.desc()).all()
	else:
		cpost = CoursePost.query.filter(CoursePost.csid == csid).filter(CoursePost.visible == 1).order_by(CoursePost.date.desc()).all()
	if cpost == None:
		return None
	cpostarr = list()
	for cp in cpost:
		cpostarr.append({"id": cp.pid, "csid": cp.csid, "title": cp.title, "date": cp.date, "edit": cp.lastedit, "text": cp.postdata})
	return cpostarr

def course_setassign(csid, assignments):
	cs = findcourse_byid(csid)
	if cs == None:
		return None
	cs.assignments = assignments
	DB.session.add(cs)
	DB.session.commit()
	return cs

def course_setgrade(csid, grades):
	cs = findcourse_byid(csid)
	if cs == None:
		return None
	cs.grades = grades
	DB.session.add(cs)
	DB.session.commit()
	return cs

def newassignment(csid, uid, path):
	cs = findcourse_byid(csid)
	if cs == None:
		return None
	user = db.finduser_byid(uid)
	if user == None:
		return None
	asm = Assignment()
	asm.csid = csid
	asm.uid = uid
	asm.path = path
	asm.read = 0
	asm.date = datetime.now()
	DB.session.add(asm)
	DB.session.commit()
	return asm

def listassignment(csid):
	asmlist = Assignment.query.filter(Assignment.csid == csid).order_by(Assignment.date.desc()).all()
	asmarr = list()
	for asm in asmlist:
		user = db.finduser_byid(asm.uid)
		asmarr.append({"id": asm.aid, "ownerid": db.user_getid(user), "owner": db.user_getname(user), "path": asm.path, "date": asm.date, "read": asm.read, "name": asm.path.rpartition('/')[2]})
	return asmarr

def findassignment_byid(aid):
	return Assignment.query.filter(Assignment.aid == aid).first()

def assignment_setasread(aid):
	asm = findassignment_byid(aid)
	asm.read = 1
	DB.session.commit()
	return asm

def addenrollment(csid, uid):
	user = db.finduser_byid(uid)
	cs = findcourse_byid(csid)
	if user == None or cs == None:
		return None
	en = Enrollment()
	en.uid = uid
	en.csid = csid
	DB.session.add(en)
	DB.session.commit()
	return en

def checkenrollment(uid, csid):
	en = Enrollment.query.filter(Enrollment.csid == csid).filter(Enrollment.uid == uid).first()
	if en == None:
		return 0
	return 1
