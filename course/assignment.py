# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.
from flask import render_template, session, request, redirect, send_file
from pepperchips.include import db, util
from course.course import capp
from course.include import cdb

@capp.route('/assignment.py')
def assignment():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	if loggeduser.uid == -1:
		return redirect("../login.py")
	csid = request.args.get("csid", -1)
	cs = cdb.findcourse_byid(csid)
	if cs == None:
		return "Unknown course id.", 404
	siteopt['csid'] = csid
	if db.user_getid(loggeduser) == cs.ownerid:
		siteopt['courseowner'] = 1
	else:
		return "You do not have access to this area.", 403
	siteopt['page'] = "assignments"
	if request.args.get("action", "") == "get":
		return getassignment()
	asmarr = cdb.listassignment(csid)
	print(asmarr)
	return render_template('course/assignment.tpl', siteopt = siteopt, linkarr = linkarr, asmarr = asmarr)

def getassignment():
	aid = request.args.get("aid", -1)
	asm = cdb.findassignment_byid(aid)
	if asm == None:
		return "Assignment not found.", 404
	user = db.finduser_byid(asm.uid)
	asm = cdb.assignment_setasread(aid)
	return send_file(asm.path, as_attachment=True, attachment_filename="(" + db.user_getname(user) + ")_" + asm.path.rpartition("/")[2])
