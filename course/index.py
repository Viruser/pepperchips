# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.
from flask import render_template, session, request, redirect
from pepperchips.include import db, util
from course.course import capp
from course.include import cdb


@capp.route('/')
@capp.route('/index.py')
def course_index():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	if loggeduser.uid == -1:
		return redirect("../login.py")
	semarr = cdb.semlist()
	if 'browse' in request.args:
		return course_browse(siteopt, loggeduser, linkarr)
	return render_template('course/index.tpl', siteopt = siteopt, linkarr = linkarr, semarr = semarr)

def course_browse(siteopt, loggeduser, linkarr):
	smid = request.args.get('sid', -1)
	csarr = cdb.courselist(smid)
	if csarr == None:
		return "Not found.", 404
	siteopt['page'] = "browse"
	return render_template('course/index.tpl', siteopt = siteopt, linkarr = linkarr, csarr = csarr)
