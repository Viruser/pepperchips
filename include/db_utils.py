# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from include import db

def populate_post(p):
	post = dict()
	post['pid'] = p.pid
	post['uid'] = p.uid
	post['catid'] = p.catid
	post['title'] = p.title
	post['postdata'] = p.postdata
	post['date'] = p.date
	post['tags'] = p.tags
	post['lastedit'] = p.lastedit
	post['staticurl'] = p.staticurl
	post['comment'] = (p.flags >> 4) & 0x0F
	user = db.finduser_byid(p.uid)
	if user != None:
		post['username'] = user.username
	else:
		post['username'] = "Nobody"
		
	post['category'] = "NoCategory"
	if p.catid != None:	
		cat = db.findcat_byid(p.catid)
		if cat != None:
			post['category'] = cat.title
	return post

def parselink(link):
	split = link.split('::')
	if split[0] != "SPECIAL":
		return link
	if split[1] == "POST":
		return "/post.py?pid=" + split[2]
		
	return link
