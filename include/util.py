# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import session
from include import db

def hashpassword(password):
	return password;

def checkpassword(password, hash):
	if password == hash:
		return 1
	return 0

def siteopt_init():
	siteopt = dict()
	if 'logged_in' in session and session['logged_in'] == 1:
		siteopt['logged_in'] = 1
		siteopt['username'] = session['username']
		siteopt['uid'] = session['uid']
	else:
		siteopt['logged_in'] = 0
		siteopt['username'] = "Guest"
		siteopt['uid'] = -1
	return siteopt

def checklogin():
	if 'logged_in' not in session or session['logged_in'] == 0:
		return db.User(uid = -1, username = "Guest", level = 0)
	return db.finduser_byid(session['uid'])
	
def linkbox_populate(maxvisible):
	boxes = db.listboxes(maxvisible)
	linkarr = { "header": None, "footer": None, "sidebar": list()}
	for box in boxes:
		links = db.listlinks(box, maxvisible)
		if db.linkbox_getpos(box) == 0:
			linkarr['header'] = { 'title': db.linkbox_gettitle(box), 'links': links }
		elif db.linkbox_getpos(box) == 1:
			linkarr['footer'] = { 'title': db.linkbox_gettitle(box), 'links': links } 
		else:
			linkarr['sidebar'].append({ 'title': db.linkbox_gettitle(box), 'links': links })
	return linkarr
