# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from include import db_utils, util

DB = SQLAlchemy()

class Post(DB.Model):
	pid = DB.Column(DB.Integer, primary_key=True)
	uid = DB.Column(DB.Integer, nullable=False)
	catid = DB.Column(DB.Integer, nullable=True)
	title = DB.Column(DB.String(256), nullable=False)
	postdata = DB.Column(DB.Text, nullable=False)
	date = DB.Column(DB.DateTime, nullable=False)
	lastedit = DB.Column(DB.DateTime, nullable=True)
	flags = DB.Column(DB.Integer, nullable=False)
	tags = DB.Column(DB.String(512), nullable=True)
	staticurl = DB.Column(DB.String(512), nullable=True)
	
class User(DB.Model):
	uid = DB.Column(DB.Integer, primary_key=True)
	username = DB.Column(DB.String(256), nullable=False)
	password = DB.Column(DB.String(256), nullable=False)
	level = DB.Column(DB.Integer, nullable=False)
	regdate = DB.Column(DB.DateTime, nullable=False)
	lastactive = DB.Column(DB.DateTime, nullable=True)
	banlevel = DB.Column(DB.Integer, nullable=False)
	bandate = DB.Column(DB.DateTime, nullable=True)
	email = DB.Column(DB.String(512), nullable=False)
	
class Comment(DB.Model):
	cid = DB.Column(DB.Integer, primary_key=True)
	pid = DB.Column(DB.Integer, nullable=False)
	uid = DB.Column(DB.Integer, nullable=True)
	commentdata = DB.Column(DB.Text, nullable=False)
	date = DB.Column(DB.DateTime, nullable=False)
	lastedit = DB.Column(DB.DateTime, nullable=True)
	visible = DB.Column(DB.Integer, nullable=False)

class Linkbox(DB.Model):
	lid = DB.Column(DB.Integer, primary_key=True)
	title = DB.Column(DB.String(256), nullable=True)
	visible = DB.Column(DB.Integer, nullable=False)
	link = DB.Column(DB.Text, nullable=True)
	pos = DB.Column(DB.Integer, nullable=False)
	boxid = DB.Column(DB.Integer, nullable=True)
	
class Category(DB.Model):
	catid = DB.Column(DB.Integer, primary_key=True)
	title = DB.Column(DB.String(256), nullable=False)


def finduser_byname(username):
	return User.query.filter(User.username == username).first()
	
def finduser_byid(uid):
	return User.query.get(uid)

def adduser(uid, username, password, email, level):
	if uid != -1:
		u = finduser_byid(uid)
		if u == None:
			return None
	else:
		u = User()

	u.username = username
	u.password = util.hashpassword(password)
	u.email = email
	u.regdate = datetime.now()
	u.level = level
	u.banlevel = 0
	if uid == -1:
		DB.session.add(u)
	DB.session.commit()
	return u

def findcat_byid(catid):
	return Category.query.get(catid)
	
def addpost(user, title, postdata, catid = None, page = 0, date = datetime.now()):
	p = Post()
	p.uid = user.uid
	p.catid = catid
	p.title = title
	p.postdata = postdata
	p.date = date
	p.flags = 0
	p.flags = p.flags | (page << 8)
	DB.session.add(p)
	DB.session.commit()
	return p
	
def addlink(box, title, link, visible = 0, pos = -1):
	if pos == -1:
		last = Linkbox.query.filter(Linkbox.boxid == box.lid).order_by(Linkbox.pos.desc()).first()
		if last == None:
			pos = 0;
		else:
			pos = last.pos + 1;
	newlink = Linkbox()
	newlink.boxid = box.lid;
	newlink.title = title
	newlink.visible = visible
	newlink.pos = pos
	newlink.link = link
	DB.session.add(newlink)
	DB.session.commit()
	return newlink
	
def addbox(title, visible, pos, link = None):
	box = Linkbox()
	box.title = title
	box.visible = visible
	box.pos = pos
	box.link = link
	DB.session.add(box)
	DB.session.commit()
	return box

def addcomment(user, post, text, date = datetime.now(), visible = 0):
	c = Comment()
	c.uid = user.uid
	c.pid = post.pid
	c.commentdata = text
	c.date = date
	c.visible = visible
	DB.session.add(c)
	DB.session.commit()
	return c

def findbox_byid(lid):
	return Linkbox.query.get(lid)
	
def findbox_bypos(pos):
	return Linkbox.query.filter(Linkbox.pos == pos, Linkbox.boxid == None).first()

def listboxes(maxvisible = 0):
	return Linkbox.query.filter(Linkbox.visible <= maxvisible, Linkbox.boxid == None).order_by(Linkbox.pos.asc()).all()
	
def listlinks(box, maxvisible = 0, asc = 1, count = 0):
	ret = list()
	if box.link != None and box.link == "SPECIAL::category":
		cats = Category.query.order_by(Category.title).all()
		for c in cats:
			ret.append({ 'lid': c.catid, 'title': c.title, 'link': "/index.py?browse&amp;catid=" + str(c.catid) })
		return ret

	query = Linkbox.query.filter(Linkbox.boxid == box.lid, Linkbox.visible <= maxvisible)
	if asc == 1:
		query = query.order_by(Linkbox.pos.asc())
	else:
		query = query.order_by(Linkbox.pos.desc())
	if count > 0:
		query = query.limit(count)
	links = query.all()
	for l in links:
		ret.append({ 'lid': l.lid, 'title': l.title, 'link': db_utils.parselink(l.link)})
	return ret

def listcomments(pid, maxvisible = 0):
	comments = Comment.query.filter(Comment.pid == pid, Comment.visible <= maxvisible).order_by(Comment.date.desc())
	return comments

def findpost_byid(pid):
	return Post.query.get(pid)
	
def listpost_byid(pid, visible = 0):
	post = Post.query.filter(Post.pid == pid).filter(Post.flags.op('&')(0x0F) <= visible).first()
	print(post.flags & 0x0F)
	if post == None:
		return None
	return db_utils.populate_post(post)
	
def listpost(maxvisible = 0, uid = -1, catid = -1, count = 0, page = 0, pagesize = 0, asc = 0):
	query = Post.query.filter(Post.flags.op('&')(0x0F) <= maxvisible).filter(Post.flags.op('&')(0x100) == 0)
	if uid != -1:
		query = query.filter(Post.uid == uid)
	if catid != -1:
		query = query.filter(Post.catid == catid)
	if asc == 1:
		query = query.order_by(Post.date.asc())
	else:
		query = query.order_by(Post.date.desc())
		
	if count > 0:
		query = query.limit(count)
	if page > 0 and pagesize > 0:
		query = query.offset(page * pagesize)
		
	posts = query.all()
	ret = list()
	
	for p in posts:
		print(p.flags)
		ret.append(db_utils.populate_post(p))
	return ret

def linkbox_getpos(box):
	return box.pos
	
def linkbox_gettitle(box):
	return box.title

def user_getlevel(user):
	return user.level

def cat_add(title):
	cat = Category(title = title)
	DB.session.add(cat)
	DB.session.commit()
	return cat
	
def cat_getid(cat):
	return cat.catid
	
def cat_gettitle(cat):
	return cat.title

def post_getid(post):
	return post.pid
	
def user_getname(user):
	return user.username

def user_getemail(user):
	return user.email
	
def user_getregdate(user):
	return user.regdate

def user_getid(user):
	return user.uid
