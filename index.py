# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.


from flask import render_template, session, redirect, request
from pepperchips import app
from include import db, util

@app.route('/')
@app.route('/index.py')
def index():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	if loggeduser.uid == None:
		return redirect("/login.py?logout")
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	
	catid = request.args.get('catid', -1)

	posts = db.listpost(maxvisible = db.user_getlevel(loggeduser), count = 5, catid = catid)
	postarr = list()
	for p in posts:
		postarr.append({ 'title': p['title'], 'postdata': p['postdata'], 'date': p['date'], 'username': p['username'], 'uid': p['uid'], 'url': "post.py?pid=" + str(p['pid']), 'cat': p['category']})
	
	return render_template('index.tpl', postarr = postarr, siteopt = siteopt, linkarr = linkarr);
