# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, session
from include import db

app = Flask(__name__) # global fask object
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://pchips_user:pchips_password@192.168.0.135/pchips'

app.secret_key = 'pchips_session_key'
app.config['UPLOAD_FOLDER'] = "/tmp/"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'zip', 'tar', 'gz'])

VERSION = "0.2.0"

from pepperchips import index,install,login,post,new,user
from course import course

app.register_blueprint(course.capp, url_prefix='/course')
db.DB.init_app(app)
