# pepperchips -- Content management system
# Copyright (C) 2017-2018 Arman Hajishafieha
#
#  This file is part of pepperchips.
#  pepperchips is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pepperchips is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pepperchips.  If not, see <http://www.gnu.org/licenses/>.

from flask import render_template, session, request, redirect
from pepperchips import app
from include import db, util

@app.route('/user.py')
def user():
	siteopt = util.siteopt_init()
	loggeduser = util.checklogin()
	if loggeduser == None:
		return redirect("/login.py?logout")
	linkarr = util.linkbox_populate(db.user_getlevel(loggeduser))
	uid = int(request.args.get("uid", 0))
	user = db.finduser_byid(uid)
	if user == None:
		return "404", 404
	if db.user_getlevel(user) == 1:
		role = "Student"
	elif db.user_getlevel(user) == 2:
		role = "Instructor"
	elif db.user_getlevel(user) >= 3:
		role = "Administrator"
	else:
		role = "unknown"
	u = dict()
	u['username'] = db.user_getname(user)
	u['email'] = db.user_getemail(user)
	u['role'] = role
	return render_template("user.tpl", siteopt = siteopt, user = u, linkarr = linkarr)
