<div class="site-sidebar">
	{% for sidebar in linkarr.sidebar %}
	<div class="site-sidebar-menu">
	<p>{{ sidebar.title }}</p>
	<ul>
		{% for link in sidebar.links %}
		<li><a href="{{ link.link }}">{{ link.title }}</a></li>
		{% endfor %}
	</ul>
	</div>
	{% endfor %}
</div>
