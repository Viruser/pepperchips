{% extends 'page.tpl' %}
{% block body %}
{% include 'header.tpl' %}
<div class="site-container">
	{% include 'sidebar.tpl' %}
	<div class="site-content">
		{% for p in postarr %}
		<div class="post-container">
			<div class="post-header">
				<h1><a href="{{ p.url }}">{{ p.title }}</a></h1>
				<p>{{ p.date }} by <a href="/user.py?uid={{ p.uid }}">{{ p.username }}</a> in {{ p.cat }}</p>
			</div>
			<p class="post-content">{{ p.postdata }}</p>
		</div>
		{% endfor %}
		<div class="post-container">
			<h2>Comments</h2><br>
			{% for c in commentarr %}
			<div class="comment">
				<p class="comment-header">{{ c.date }} by {{ c.username }}</p>
				<p class="comment-content">{{ c.commentdata }}</p>
			</div>
			{% endfor %}
			<h2>New comment</h2><br>
			<p>logged in as: {{ siteopt.username }}</p>
			<form method="POST">
				<textarea name="comment" cols="40" rows="5"></textarea><br>
				<input type="submit" name="submit" value="Send">
			</form>
		</div>
	</div>
</div>
{% include 'footer.tpl' %}
{% endblock %}
