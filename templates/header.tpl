<header>
<div class="site-header-img">
	<a href="/"><img src="/static/res/header.png"></a>
</div>
<div class="site-header-links">
	<ul>
		{% for link in linkarr.header.links %}
		<li class="left"><a href="{{ link.link }}">{{ link.title }}</a></li>
		{% endfor %}
		{% if siteopt.logged_in == 1 %}
		<li class="right"><a href="/login.py?logout">logout</a></li>
		<li class="right"><a href="/user.py?uid={{ siteopt.uid }}">{{ siteopt.username }}</a></li>
		{% else %}
		<li class="right"><a href="/login.py">login/register</a></li>
		{% endif %}
	</ul>
</div>
</header>
