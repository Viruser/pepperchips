{% extends 'page.tpl' %}
{% block body %}
{% include 'header.tpl' %}
<div class="site-container">
	{% include 'sidebar.tpl' %}
	<div class="site-content">
		{% for p in postarr %}
		<div class="post-container">
			<div class="post-header">
				<h1><a href="{{ p.url }}">{{ p.title }}</a></h1>
				<p>{{ p.date }} by <a href="/user.py?uid={{ p.uid }}">{{ p.username }}</a> in {{ p.cat }}</p>
			</div>
			<p class="post-content">{{ p.postdata }}</p>
		</div>
		{% endfor %}
	</div>
</div>
{% include 'footer.tpl' %}
{% endblock %}
