{% extends 'page.tpl' %}
{% block body %}
{% include 'header.tpl' %}
<div class="site-container">
	{% include 'sidebar.tpl' %}
	<div class="site-content">
		<div class="post-container">
			<div class="post-header">
				<h1>User profile: {{ user.username }}</h1>
			</div>
			<p class="post-content">E-mail: {{ user.email }}<br>Role: {{ user.role }}</p>
		</div>
	</div>
</div>
{% include 'footer.tpl' %}
{% endblock %}
