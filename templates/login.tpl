{% extends 'page.tpl' %}
{% block body %}
{% include 'header.tpl' %}
<div class="site-container">
	<div class="form-box-parent">
	<div class="form-box-child">
	<h2>Login</h2>
	{% if form.error_num == 1 %}
	<p class="form-box-error">{{ form.error }}</p>
	{% endif %}
	<form submit="/login.py" method="POST">
		<table>
		<tr>
			<td><label for="username">Username:</label></td><td><input id="username" type="text" name="username" value="{{ form.username }}"></td>
		</tr>
		<tr>
			<td><label for="password">Password:</label></td><td><input id="password" type="password" name="password"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="login"></td>
		</tr>
		</table>
	</form>
	</div>
	<div class="form-box-child">
	<h2>Register</h2>
	{% if form.error_num == 2 %}
	<p class="form-box-error">{{ form.error }}</p>
	{% endif %}
	<form submit="/login.py" method="POST">
		<table>
		<tr>
			<td><label for="username">Username:</label></td><td><input id="username" type="text" name="username" value="{{ form.username }}"></td>
		</tr>
		<tr>
			<td><label for="password">Password:</label></td><td><input id="password" type="password" name="password"></td>
		</tr>
		<tr>
			<td><label for="password2">Repeat Password:</label></td><td><input id="password2" type="password" name="password2"></td>
		</tr>
		<tr>
			<td><label for="username">E-mail:</label></td><td><input id="email" type="text" name="email" value="{{ form.email }}"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="register"></td>
		</tr>
		</table>
	</form>
	</div>
	</div>
</div>
{% include 'footer.tpl' %}
{% endblock %}
