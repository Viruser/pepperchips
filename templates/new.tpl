{% extends 'page.tpl' %}
{% block body %}
{% include 'header.tpl' %}
<div class="site-container">
	{% include 'sidebar.tpl' %}
	<div class="site-content">
		{% if form.action == "post" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>New Post</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="title">Title:</label></td><td><input id="title" type="text" name="title" value="{{ form.title }}"></td>
			</tr>
			<tr>
				<td><label for="cat">Category id:</label></td><td><input id="cat" type="text" name="cat" value="{{ form.cat }}"></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name="text" cols="60" rows="10">{{ form.text }}</textarea></td>
			</tr>
			<tr>
				<td><input id="page" type="checkbox" name="page" value="page" {% if form.page %}checked{% endif %}> <label for="page">Page</label></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="New post"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
		{% endif %}
		{% if form.action == "user" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>Add/edit User</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="uid">uid:</label></td><td><input id="uid" type="text" name="uid" value="{{ form.uid }}"></td>
			</tr>
			<tr>
				<td><label for="username">Username:</label></td><td><input id="username" type="text" name="username" value="{{ form.username }}"></td>
			</tr>
			<tr>
				<td><label for="password">Password:</label></td><td><input id="password" type="text" name="password" value="{{ form.password }}"></td>
			</tr>
			<tr>
				<td><label for="password2">Repeat Password:</label></td><td><input id="password2" type="text" name="password2" value="{{ form.password2 }}"></td>
			</tr>
			<tr>
				<td><label for="level">level:</label></td><td><input id="level" type="text" name="level" value="{{ form.level }}"></td>
			</tr>
			<tr>
				<td><label for="email">Email:</label></td><td><input id="email" type="text" name="email" value="{{ form.email }}"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="Add/edit user"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
		{% endif %}
		{% if form.action == "link" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>New link</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="title">Title:</label></td><td><input id="title" type="text" name="title" value="{{ form.title }}"></td>
			</tr>
			<tr>
				<td><label for="box">linkbox id:</label></td><td><input id="box" type="text" name="box" value="{{ form.box }}"></td>
			</tr>
			<tr>
				<td><label for="link">link:</label></td><td><input id="link" type="text" name="link" value="{{ form.link }}"></td>
			</tr>
			<tr>
				<td><label for="pos">Position:</label></td><td><input id="pos" type="text" name="pos" value="{{ form.pos }}"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="New link"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
		{% endif %}
	</div>
</div>
{% include 'footer.tpl' %}
{% endblock %}
