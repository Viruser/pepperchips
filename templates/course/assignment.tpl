{% extends '/page.tpl' %}
{% block body %}
{% include '/header.tpl' %}
<div class="site-container">
	{% include '/course/sidebar.tpl' %}
	<div class="site-content">
		<div class="post-container">
			<div class="post-header">
				<h1>List of Unread assignments</h1>
			</div>
			<p class="post-content">
				{% if asmarr %}
				{% for asm in asmarr %}
					{% if asm.read == 0 %}
					<a href="assignment.py?action=get&amp;csid={{ siteopt.csid }}&amp;aid={{ asm.id }}">{{ asm.name }}</a>
					submitted by: <a href="/user.py?uid={{ asm.ownerid }}">{{ asm.owner }}</a> at {{ asm.date }}<br>
					{% endif %}
				{% endfor %}
				{% else %}
				No assignment has been submitted yet.
				{% endif %}
			</p>
		</div>
		<div class="post-container">
			<div class="post-header">
				<h1>List of Read assignments</h1>
			</div>
			<p class="post-content">
				{% if asmarr %}
				{% for asm in asmarr %}
					{% if asm.read == 1 %}
					<a href="assignment.py?action=get&amp;csid={{ siteopt.csid }}&amp;aid={{ asm.id }}">{{ asm.name }}</a>
					submitted by: <a href="/user.py?uid={{ asm.ownerid }}">{{ asm.owner }}</a> at {{ asm.date }}<br>
					{% endif %}
				{% endfor %}
				{% else %}
				No assignment has been submitted yet.
				{% endif %}
			</p>
		</div>
	</div>
</div>
{% include '/footer.tpl' %}
{% endblock %}
