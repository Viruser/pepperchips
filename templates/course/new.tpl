{% extends 'page.tpl' %}
{% block body %}
{% include 'header.tpl' %}
<div class="site-container">
	{% include '/course/sidebar.tpl' %}
	<div class="site-content">
	{% if form.action == "course" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>New course</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="title">Title:</label></td><td><input id="title" type="text" name="title" value="{{ form.title }}"></td>
			</tr>
			<tr>
				<td><label for="oid">professor id:</label></td><td><input id="oid" type="text" name="oid" value="{{ form.oid }}"></td>
			</tr>
			<tr>
				<td><label for="smid">semester id:</label></td><td><input id="link" type="text" name="smid" value="{{ form.smid }}"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="New Course"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
	{% endif %}
	{% if form.action == "enrollment" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>New course</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="uid">User id:</label></td><td><input id="uid" type="text" name="uid" value="{{ form.uid }}"></td>
			</tr>
			<tr>
				<td><label for="csid">Course id:</label></td><td><input id="csid" type="text" name="csid" value="{{ form.csid }}"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="New Enrollment"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
	{% endif %}
	{% if form.action == "semester" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>New course</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="title">Semester number:</label></td><td><input id="title" type="text" name="title" value="{{ form.title }}"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="New semester"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
	{% endif %}
	{% if form.action == "news" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		<h2>Add/edit news</h2>
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td><label for="nid">news id:</label></td><td><input id="nid" type="text" name="nid" value="{{ form.nid }}"></td>
			</tr>
			<tr>
				<td><label for="title">Title:</label></td><td><input id="title" type="text" name="title" value="{{ form.title }}"></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name="text" cols="60" rows="10">{{ form.text }}</textarea></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="Submit"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
	{% elif form.action == "assignments" or form.action == "grades" %}
		<div class="form-box-parent">
		<div class="form-box-child">
		{% if form.action == "assignments" %}
		<h2>Assignments</h2>
		{% else %}
		<h2>Grades</h2>
		{% endif %}
		{% if form.error_num == 1 %}
		<p class="form-box-error">{{ form.error }}</p>
		{% endif %}
		<form method="POST">
			<table>
			<tr>
				<td colspan="2"><textarea name="text" cols="60" rows="10">{{ form.text }}</textarea></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="Submit"></td>
			</tr>
			</table>
		</form>
		</div>
		</div>
	{% endif %}
	</div>
</div>
{% include 'footer.tpl' %}
{% endblock %}
