	<div class="site-sidebar">
		{% if siteopt.courseowner == 1 %}
		<div class="site-sidebar-menu">
		<p>Professor options</p>
		<ul>
			<li><a href="new.py?csid={{ siteopt.csid }}&amp;action=news">Add/edit News</a></li>
			<li><a href="assignment.py?csid={{ siteopt.csid }}">Get Assignments</a></li>
		</ul>
		</div>
		{% endif %}
		<div class="site-sidebar-menu">
		<p>Course actions</p>
		<ul>
			<li><a href="course.py?csid={{ siteopt.csid }}&amp;action=news">News</a></li>
			<li><a href="course.py?csid={{ siteopt.csid }}&amp;action=grades">Grades</a></li>
			<li><a href="course.py?csid={{ siteopt.csid }}&amp;action=assignments">Assignments</a></li>
		</ul>
	</div>
	</div>
