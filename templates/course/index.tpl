{% extends '/page.tpl' %}
{% block body %}
{% include '/header.tpl' %}
<div class="site-container">
	{% include '/sidebar.tpl' %}
	<div class="site-content">
		<div class="post-container">
			<div class="post-header">
				{% if siteopt.page == "browse" %}
				<h1>List of courses</h1>
				{% elif siteopt.page == "assignments" %}
				<h1>List of submitted assignments</h1>
				{% else %}
				<h1>List of semesters</h1>
				{% endif %}
			</div>
			<p class="post-content">
				{% if semarr %}
					{% for s in semarr %}
					<a href="index.py?browse&amp;sid={{ s.id }}">{{ s.number }}</a><br>
					{% endfor %}
				{% elif csarr %}
					{% for cs in csarr %}
					<a href="course.py?csid={{ cs.id }}">{{ cs.title }}</a> by: {{ cs.owner }}<br>
					{% endfor %}
				{% elif asmarr %}
					{% for asm in asmarr %}
						{% if asm.read == 0 %}
						<div class="strong"><a href="assignment.py?action=get&amp;aid={{ asm.id }}">{{ asm.name }}</a> submitted by: {{ asm.owner }} at {{ asm.date }}</div>
						{% else %}
						<div><a href="assignment.py?action=get&amp;aid={{ asm.id }}">{{ asm.name }}</a> submitted by: {{ asm.owner }} at {{ asm.date }}</div>
						{% endif %}
					{% endfor %}
				{% else %}
				Nothing found.
				{% endif %}
			</p>
		</div>
	</div>
</div>
{% include '/footer.tpl' %}
{% endblock %}
