{% extends '/page.tpl' %}
{% block body %}
{% include '/header.tpl' %}
<div class="site-container">
	{% include '/course/sidebar.tpl' %}
	<div class="site-content">
	{% if siteopt.page == "news" %}
		{% if postarr %}
			{% for p in postarr %}
		<div class="post-container">
			<div class="post-header">
				<h1>{{ p.title }}</h1>
				<p>{{ p.date }}</p>
			</div>
			<p class="post-content">
				{{ p.text }}
			</p>
		</div>
		{% endfor %}
		{% else %}
		<div class="post-container">
			<div class="post-header">
				<h1>Course news</h1>
			</div>
			<p class="post-content">
				No news yet.
			</p>
		</div>
		{% endif %}
	{% else %}
	<div class="post-container">
		<div class="post-header">
			{% if siteopt.page == "grades" %}
			<h1>Course grades</h1>
			{% if siteopt.courseowner %}
				<p><a href="new.py?action=grades&amp;csid={{ siteopt.csid }}">edit</a></p>
			{% endif %}
			{% elif siteopt.page == "assignments" %}
			<h1>Assignments</h1>
			{% if siteopt.courseowner %}
				<p><a href="new.py?action=assignments&amp;csid={{ siteopt.csid }}">edit</a></p>
			{% endif %}
			{% else %}
			<h1>Course news</h1>
			{% endif %}
		</div>
		<p class="post-content">
			{{ post.text }}
		</p>
	</div>
	{% if siteopt.page == "assignments" and siteopt.form %}
	<div class="post-container">
		<h2>Submit assignment</h2>
		<p>logged in as: {{ siteopt.username }}</p><br>
		{% if form and form.error %}
		<p>{{ form.error }}</p>
		{% endif %}
		<form method="POST" enctype="multipart/form-data">
			<input name="assignment" type="file" required>
			<input type="submit" name="submit" value="Send">
		</form>
	</div>
	{% endif %}
	{% endif %}
	</div>
</div>
{% include '/footer.tpl' %}
{% endblock %}
